customers.controller("customersController", function ($scope, $http) 
{
    $scope.createWidget = false;
    $http({
            method: 'get',
            url: 'http://localhost:8000/customers'
    }).success(function (data, status) 
	{
                
		var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'customerName', type: 'string' },
                        { name: 'contactFirstName', type: 'string' },
                        { name: 'contactLastName', type: 'int' },
                        { name: 'phone', type: 'string' },
                        { name: 'addressLine1', type: 'string' }
                    ],
                    id: 'id',
                    localdata: data
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $scope.gridSettings =
                {
					pageable: true,
					autowidth: true,
					autoheight: true,
                    source: dataAdapter,
                    columnsresize: true,
                    columns: [
                      { text: 'Customer Name', datafield: 'customerName', width: 250 },
                      { text: 'Contact First Name', datafield: 'contactFirstName', width: 250 },
                      { text: 'Contact Last Name', datafield: 'contactLastName', width: 180 },
                      { text: 'Phone', datafield: 'phone', width: 200 },
                      { text: 'Address Line', datafield: 'addressLine1', width: 400 }
                    ]
                };
                $scope.createWidget = true;
    }).error(function (data, status) {
                
    });
});