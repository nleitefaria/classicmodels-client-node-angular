employees.controller("employeesController", function ($scope, $http) 
{
    $scope.createWidget = false;
    $http({
            method: 'get',
            url: 'http://localhost:8000/employees'
    }).success(function (data, status) 
	{
                
		var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'firstName', type: 'string' },
                        { name: 'lastName', type: 'string' },
                        { name: 'extension', type: 'int' },
                        { name: 'email', type: 'string' },
                        { name: 'jobTitle', type: 'string' }
                    ],
                    id: 'id',
                    localdata: data
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $scope.gridSettings =
                {
					pageable: true,
					autowidth: true,
					autoheight: true,
                    source: dataAdapter,
                    columnsresize: true,
                    columns: [
                      { text: 'First Name', datafield: 'firstName', width: 250 },
                      { text: 'Last Name', datafield: 'lastName', width: 250 },
                      { text: 'Extension', datafield: 'extension', width: 180 },
                      { text: 'Email', datafield: 'email', width: 250 },
                      { text: 'Job Title', datafield: 'jobTitle', width: 400 }
                    ]
                };
                $scope.createWidget = true;
    }).error(function (data, status) {
                
    });
});